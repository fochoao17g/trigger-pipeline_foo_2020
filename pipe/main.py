import enum
import time
import yaml
import os

import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_logger

BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'

logger = get_logger()

schema = {'BITBUCKET_USERNAME': {'required': True, 'type': 'string'},
          'BITBUCKET_APP_PASSWORD': {'required': True, 'type': 'string'},
          'ACCOUNT': {'nullable': True, 'required': False, 'type': 'string',
                      'default': os.getenv('BITBUCKET_REPO_OWNER')},
          'BRANCH_NAME': {'type': 'string', 'required': False, 'default': 'master'},
          'REF_TYPE': {'allowed': ['branch', 'tag'], 'default': 'branch', 'type': 'string'},
          'CUSTOM_PIPELINE_NAME': {
              'type': 'string',
              'required': False,
              'nullable': True,
              'default': None},
          'PIPELINE_VARIABLES': {
              'required': False,
              'nullable': True,
              'default': None},
          'REPOSITORY': {'required': True, 'type': 'string'},
          'WAIT': {'default': False, 'type': 'boolean'},
          'WAIT_MAX_TIMEOUT': {'default': 3600, 'type': 'integer'},
          'DEBUG': {'default': False, 'type': 'boolean'}}


@enum.unique
class BuildStatus(enum.Enum):
    completed = 'COMPLETED'
    succeeded = 'SUCCESSFUL'
    failed = 'FAILED'
    pending = 'PENDING'
    in_progress = 'IN_PROGRESS'
    paused = 'PAUSED'
    halted = 'HALTED'


class TriggerBuild(Pipe):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.username = self.get_variable('BITBUCKET_USERNAME')
        self.account = self.get_variable('ACCOUNT')
        self.repo = self.get_variable('REPOSITORY')
        self.ref_name = self.get_variable('BRANCH_NAME')

    def _check_build(self, build_number, auth):
        response = requests.get(
            f'{BITBUCKET_API_BASE_URL}/repositories/{self.account}/{self.repo}/pipelines/{build_number}', auth=auth)
        if not response.status_code == 200:
            message = (f"Failed to get the build state for build number {build_number} for {self.repo}. "
                       f"Status Code: {response.status_code}. Error: {response.text}")
            self.log_error(f"{message}. Retrying...")
            return None, message

        build_data = response.json()
        build_state = build_data['state']['name']

        logger.debug(f"Build state: {build_data['state']}")

        if build_state == BuildStatus.completed.value:
            result = build_data['state']['result']['name']

            if result == BuildStatus.succeeded.value:
                return True, f"Build number {build_number} for {self.repo}@{self.ref_name} finished successfully."
            elif result == BuildStatus.failed.value:
                return False, f"Build number {build_number} for {self.repo}@{self.ref_name} failed."
            else:
                return False, (f"Build number {build_number} for {self.repo}@{self.ref_name} "
                               f"finished with result {result}.")

        elif build_state in (BuildStatus.pending.value, BuildStatus.in_progress.value):
            logger.info(
                f"Build number {build_number} for {self.repo} is {build_state}, "
                f"waiting for pipeline to finish...")

            if build_data['state']['stage']['name'] == BuildStatus.paused.value:
                return True, f"Build number {build_number} for {self.repo}@{self.ref_name} is paused."
            elif build_data['state']['stage']['name'] == BuildStatus.halted.value:
                return False, f"Build number {build_number} for {self.repo}@{self.ref_name} is halted."
            else:
                return None, None

    def _get_tag_info(self, tagname, account, repo, auth):
        logger.info(f'Fetching the tag info for tag:{tagname}')
        response = requests.get(
            f'{BITBUCKET_API_BASE_URL}/repositories/{account}/{repo}/refs/tags/{tagname}', auth=auth)
        if not response.status_code == 200:
            self.fail(
                f'Failed to get the tag info for {tagname} from Bitbucket. Response code: {response.status_code}. Error: {response.text}')
        return response.json()

    def _get_tag_target(self, tagname, username, repo, auth):
        return self._get_tag_info(tagname, username, repo, auth)['target']['hash']

    def run(self):
        super().run()

        token = self.get_variable('BITBUCKET_APP_PASSWORD')
        custom_pipeline = self.get_variable('CUSTOM_PIPELINE_NAME')
        variables = self.get_variable('PIPELINE_VARIABLES')
        wait = self.get_variable('WAIT')
        wait_max_time = self.get_variable('WAIT_MAX_TIMEOUT')

        headers = {'Content-Type': 'application/json'}

        auth = HTTPBasicAuth(self.username, token)

        data = {
            "target": {
                "ref_type": 'branch',
                "type": "pipeline_ref_target",
                "ref_name": self.ref_name
            }
        }

        if custom_pipeline is not None:
            data['target'].update({
                "selector": {
                    "type": 'custom',
                    "pattern": custom_pipeline
                }
            })

        if variables is not None:
            data.update({
                'variables': variables

            })

        response = requests.post(
            f'{BITBUCKET_API_BASE_URL}/repositories/{self.account}/{self.repo}/pipelines/', headers=headers, auth=auth, json=data)
        if response.status_code == 404:
            self.fail(f"Account, repository or branch doesn't exist.")
        elif response.status_code == 400:
            self.fail(f"Error: {response.text}")
        elif response.status_code == 401:
            self.fail(
                f"API request failed with status 401. Check your account and app password and try again.")
        elif response.status_code != 201:
            self.fail(
                f"Failed to initiate a pipeline. API error code: {response.status_code}. Message: {response.text}")
        else:
            response_data = response.json()
            build_number = response_data['build_number']
            self.success(f"Build started successfully. Build number: {build_number}."
                         f"\n\tFollow build logs at the following URL: https://bitbucket.org/"
                         f"{self.account}/{self.repo}/addon/pipelines/home#!/results/{build_number}")

        if not wait:
            self.success(f"Pipe finished successfully.", do_exit=True)

        logger.info(f"Waiting for pipeline to finish")

        deadline = time.time() + wait_max_time
        attempt = 0

        message = ""
        while time.time() < deadline:
            attempt += 1
            logger.info(f"Attempt number {attempt}: Getting build info.")

            success, message = self._check_build(build_number, auth)

            if success is None:
                time.sleep(5)
            elif not success:
                self.fail(message)
            elif success:
                self.success(message)
                break

        else:
            self.fail(f'Timeout waiting for the pipeline completion. Error: {message}')

        self.success('Pipe finished successfully.')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = TriggerBuild(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
