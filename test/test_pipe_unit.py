from contextlib import contextmanager
import io
import os
import sys
from unittest import mock

from bitbucket_pipes_toolkit.test import PipeTestCase

from pipe.main import TriggerBuild, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


@mock.patch('requests.post')
@mock.patch('requests.get')
@mock.patch.dict(os.environ, {'REPOSITORY': 'test-trigger-build', 'WAIT': 'true'})
class StatusBuildTestCase(PipeTestCase):

    def test_build_succeeded(self, build_status_mock, build_trigger_mock):
        build_trigger_mock.return_value.status_code = 201
        build_trigger_mock.return_value.json.return_value = {'build_number': 1}

        build_status_mock.return_value.status_code = 200
        build_status_mock.return_value.json.return_value = {
            'state': {'name': 'COMPLETED',
                      'type': 'pipeline_state_completed',
                      'result': {
                          'name': 'SUCCESSFUL',
                          'type': 'pipeline_state_completed_successful'}
                      }
        }

        with capture_output() as out:
            TriggerBuild(schema=schema, check_for_newer_version=True).run()

        self.assertRegex(out.getvalue(),
                         rf'✔ Build number \d+ for [-\w]+@master finished successfully.')
        self.assertRegex(out.getvalue(), f'✔ Pipe finished successfully')

    def test_build_failed(self, build_status_mock, build_trigger_mock):
        build_trigger_mock.return_value.status_code = 201
        build_trigger_mock.return_value.json.return_value = {'build_number': 1}

        build_status_mock.return_value.status_code = 200
        build_status_mock.return_value.json.return_value = {
            'state': {'name': 'COMPLETED',
                      'type': 'pipeline_state_completed',
                      'result': {
                          'name': 'FAILED',
                          'type': 'pipeline_state_completed_failed'}
                      }
        }
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), rf'✖ Build number \d+ for [-\w]+@master failed.')

    def test_build_error(self, build_status_mock, build_trigger_mock):
        build_trigger_mock.return_value.status_code = 201
        build_trigger_mock.return_value.json.return_value = {'build_number': 1}

        build_status_mock.return_value.status_code = 200
        build_status_mock.return_value.json.return_value = {'state': {
            'type': 'pipeline_state_completed',
            'name': 'COMPLETED',
            'result': {
                'type': 'pipeline_state_completed_error',
                'name': 'ERROR'
            }
        }}

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), rf'✖ Build number \d+ for [-\w]+@master finished with result ERROR.')

    @mock.patch.dict(os.environ, {'REPOSITORY': 'test-trigger-build', 'WAIT': 'true', 'WAIT_MAX_TIMEOUT': '10'})
    def test_get_build_status_retry_timeout(self, build_status_mock, build_trigger_mock):
        build_trigger_mock.return_value.status_code = 201
        build_trigger_mock.return_value.json.return_value = {'build_number': 1}

        build_status_mock.return_value.status_code = 500
        build_status_mock.return_value.text = "Internal Server Error"

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), (rf'✖ Timeout waiting for the pipeline completion. '
                                          rf'Error: Failed to get the build state for build number 1 '
                                          rf'for test-trigger-build. Status Code: 500. Error: Internal Server Error.'))

    @mock.patch.dict(os.environ, {'REPOSITORY': 'test-trigger-build', 'WAIT': 'true', 'WAIT_MAX_TIMEOUT': '10'})
    def test_get_build_status_retry_success(self, build_status_mock, build_trigger_mock):
        build_trigger_mock.return_value.status_code = 201
        build_trigger_mock.return_value.json.return_value = {'build_number': 1}

        success_mock = mock.MagicMock(status_code=200)
        success_mock.json.return_value = {
            'state': {'name': 'COMPLETED',
                      'type': 'pipeline_state_completed',
                      'result': {
                          'name': 'SUCCESSFUL',
                          'type': 'pipeline_state_completed_successful'}
                      }
        }
        build_status_mock.side_effect = [mock.MagicMock(status_code=500, text="Error"), success_mock]
        build_status_mock.return_value.text = "Internal Server Error"
        with capture_output() as out:
            TriggerBuild(schema=schema, check_for_newer_version=True).run()
        self.assertEqual(build_status_mock.call_count, 2)
        self.assertRegex(out.getvalue(),
                         rf'✔ Build number \d+ for [-\w]+@master finished successfully.')
        self.assertRegex(out.getvalue(), f'✔ Pipe finished successfully')
